<?php
//require_once 'MailUpWsImport.class.php';

class MailUpClient {

  protected $wsImport;
  protected $wsSend;
  
  /**
   *  Configuration associative array.
   *
   * 'mailup_account_user'   => Console Username
   * 'mailup_account_pass'   => Console Password
   * 'mailup_console_id'     => Console ID
   * 'mailup_api_endpoint'   => HTTP Endpoint
   * 'mailup_ws_import_wsdl' => IMPORT Webservices WSDL
   * 'mailup_ws_import_user' => Webservices Username
   * 'mailup_ws_import_pass' => Webservices Password
   * 'mailup_ws_namespace'   => Soap Namespace
   * 'mailup_ws_send_wsdl'   => SEND Webservices WSDL
   * 'mailup_confirm'        => Confirm e-mail,
  */  
  protected $configuration;

  /**
   * Library setup and initialization
   */     
  function configure ($configuration) {
    $this->configuration = $configuration;
    $this->wsSend = new MailUpSend(); 
    $this->wsImport = new MailUpWsImport(); 
    $success = $this->wsSend->setConfiguration($configuration) && $this->wsImport->setConfiguration($configuration);
    if (!$success)
      throw new MailUpException('Cannot initialize remote client. Please check your configuration.');
  }
  
  /**
   *  Retrieves lists
   *    @todo add filters
   *  @return array
   *    Lists associative array.
   */     
  function getLists () {
    $client = $this->wsImport;
    $response = $client->invoke('GetNlLists');
    
    $lists = $response->mailupBody->Lists->List;
    $returnCode = intval($response->mailupBody->ReturnCode);

    if (empty($lists)) {
      return array (
        'lists' => array(),
        'listsByGuid' => array(),
        'returnCode' => $returnCode
      );
    }
    
    $resultLists = array();
    foreach ($lists as $list) {
      $current = array (
        'id' => (int)$list['idList'],
        'name' => trim((string)$list['listName']),
        'guid' => trim((string)$list['listGUID']),
        'groups' => array(),
      );
      if ($list->Groups->Group) {
        foreach ($list->Groups->Group as $group) {
          $current['groups'][(int)$group['idGroup']]['id'] = (int)$group['idGroup'];
          $current['groups'][(int)$group['idGroup']]['name'] = (string)$group['groupName'];
        }
      }
      $resultLists [$current['id']] = $current;
      $listsByGuid [$current['guid']] = $current;
    }
    return array (
      'lists' => $resultLists,
      'listsByGuid' => $listsByGuid,
      'returnCode' => $returnCode
    );

  }
  
  function getListById ($id) {
    $lists = $this->getLists();
    $list = $lists['lists'][$id];
    
    return array (
      'list' => $list,
      'returnCode' => 0,
    );
  }
  
  function getListByGuid ($guid) {
    $lists = $this->getLists();
    $list = $lists['listsByGuid'][$guid];
    
    return array (
      'list' => $list,
      'returnCode' => 0,
    );
  }
  
  function createList ($name) {
    $client = $this->wsSend;
    $parameters = array(
      'name' => $name,
      'autoId'=>''
    );
    $response = $client->invoke('CreateList',$parameters);
    // $response = simplexml_load_string($response);
    
    return array (
      'id' => (int)$response->Lists->List['Id'],
      'guid' => (string)$response->Lists->List['Guid'],
      'name' => (string)$response->Lists->List['Name'],
      'success' => true,
    );
  }
  
  function deleteList ($id) {
    $client = $this->wsSend;
    $parameters = array(
      'listId' => $id,
      'autoId'=>''
    );
    $response = $client->invoke('DeleteList',$parameters);
    
    // @todo
    // if ($response->Error != null)
    //  return false;
      
    return true;    
    
  }
  
  function importUsers ($users,$lists,$params = array()) {
    if (count($users) == 0) 
      return false;
    
    $xml = $this->generateExportable($users, 'xml');
    
    if ($xml == "")
      return false;
    
    if (count($lists) == 0)
      return false;
    
    $listIDs = array();
    $listGUIDs = array();
    foreach ($lists as $list) {
      $listIDs [] = $list['id'];
      $listGUIDs [] = $list['guid'];
      $groups = array();
      foreach ($list['groups'] as $group_id => $group) {
        $groups [] = $group_id;
      }
      $listGroups [] = implode(',', $groups);
    }
    
    if (count($listIDs) == 0 || count($listGUIDs) == 0)
      return false;
      
    $listIDs = implode(';', $listIDs);
    $listGUIDs = implode(';', $listGUIDs);
    $listGroups = implode(';', $listGroups);
    
    $defaults = array (
      "groupsIDs" => '1', 
      'importType' => '3',
      'mobileInputType' => '2',
      'asPending' => '',
      'ConfirmEmail' => '0',
      'asOptOut' => '0',
      'forceOptIn' => '0',
      'replaceGroups' => '0',
      'idConfirmNL' => '0',
    );

    $params['listsIDs'] = $listIDs;
    $params['listsGUIDs'] = $listGUIDs;
    $params['groupsIDs']= $listGroups;
    $params['xmlDoc'] = $xml;
    
    $data = $params + $defaults;

    $client = $this->wsImport;
    $response = $client->invoke('StartImportProcesses', $data);
    
    if ($response == null)
      return null;
      
    return array (
      'success' => ((int)$response->mailupBody->processes->process->ReturnCode) == 0,
      'returnCode' => (int)$response->mailupBody->processes->process->ReturnCode,
      'processId' => (int)$response->mailupBody->processes->process->processID,
      'listID' => (int)$response->mailupBody->processes->process->listID,
    );
  }
  
  /**
   *  Generate export content. Each supported format is usable for MailUp console import.
   *  @param array $users
   *    The users list. Each user is a stdClass object defining at least
   *    $user->mail and $user->name properties.
   *  @param string $type
   *    Exportable format, can be 'xml' or 'csv'.
   *  @return string
   *    The export content.
   */              
  function generateExportable ($users, $type = 'xml') {
    $data = '';
    // We iterate through every user and add fields to the output string.
    foreach ($users as $account) {
      if ($type == 'xml') {
        $data .= "\t<subscriber email=\"{$account->mail}\" Name=\"{$account->name}\">";
        if (!empty($account->customFields)) {
          foreach ($account->customFields as $id => $value) {
            $data .= "\n\t\t<campo$id>$value</campo$id>";
          }
        }
        $data .= "\n\t</subscriber>\n";
      } else if ($type == 'csv') {
        $row = array();
        $row[] = "{$account->mail}";
        if (!empty($account->customFields)) {
          foreach ($account->customFields as $value) {
            $row[] = $value;
          }
        }
        $row = implode(";",$row);
        $data[] = $row;
      }
    }
    // Finalize basing on the output format.
    if ($type == 'xml') {
      $data = "<subscribers>\n$data</subscribers>";
    } else if ($type == 'csv') {
      $data = implode("\n",$data);
    }
    return $data;
  }

  /**
   *  Query for user fields.
   *  @return array
   *    Each array item has 'id' and 'name' fields. An empty array returns
   *    if some error occurrs.
   */
  public function getProfileFields() {
    $response = $this->wsSend->invoke('GetFields_st');
    if ($response && $response->errorCode == 0) {
      foreach ($response->Fields->Field as $field) {
        $fields[] = array (
          'id' => (int)$field['Id'],
          'name' => (string)$field['Name'],
        );
      }
      return $fields;
    }
    return array();
  }

  function getGroup ($list) {
  
  }
  
  function subscribeUser ($user) {
  
  }
  
  function unsubscribeUser ($user) {
  
  }
  
  
}
?>