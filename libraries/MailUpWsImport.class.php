<?php
//require_once 'MailUp.class.php';

/**
 * Client class for MailUpWsImport Webservice
 */
class MailUpWsImport extends MailUp {

  protected $namespace;
	
	function initialize () {
    $headers = array("User" => $this->configuration['mailup_ws_import_user'], "Password" => $this->configuration['mailup_ws_import_pass']);    
    
    $header = new SOAPHeader($this->configuration['mailup_ws_namespace'], "Authentication", $headers);
		$this->client = new SoapClient($this->configuration['mailup_ws_import_wsdl'], array("trace" => 1, "exceptions" => 1));
		$this->client->__setSoapHeaders($header);
		
		return $this->initialized = true;
  }
  
  /**
   * Post invoke hook
   */     
  protected function postInvoke (&$response,$method,$parameters = null, $raw = false) {
    $response = $this->parseResponse($response);
  }
  
  /**
   *  Parse MailUpWsImport SOAP response.
   *  @param string $soap_response
   *    The SOAP xml response.
   *  @throws MailUpException
   *    If xml response cant be parsed.
   *  @return SimpleXMLElement
   *    The parsed response.
   */      
  protected function parseResponse ($soap_response) {
    $xml = new SimpleXMLElement($soap_response);
    // Two steps inside to get the response root
    $elements = $xml->children('soap', true)->children()->children();
    // Iteration through children, only one expected.
    foreach ($elements as $e) {
      //  ----------------------------------------------------------------------
      //  @fixme DOUBLE QUOTES
      //  The xml response may contain this syntax error, double quotes inside
      //  one xml attribute (i.e. the list name). There is no way to detect the
      //  bad quotes, so we can't handle this... but keep it in mind.
      //  ----------------------------------------------------------------------
      // User input may contain the ampersand character, even if it's not a
      // valid xml one for Character Data. We replace them with the html entity.
      // @see http://www.w3.org/TR/xml/#syntax
      // @see http://www.microshell.com/programming/php/xml-and-ampersand/
      $e = preg_replace("/&/", "&amp;", $e);
      // Now we create a new simple xml based on this item, that is a nested xml
      // document.
      libxml_use_internal_errors(true);
      $response = simplexml_load_string($e);
      $errors = libxml_get_errors();
/*
// @todo enable/disable logging.
$fp = fopen('/usr/local/apache/share/htdocs/drupal6-tdd/modules/mailup_integration/log.xml', 'w');
fwrite($fp, $e);
fclose($fp);
*/
      if (empty($errors)) {
        // @todo improve error code parse.
        if ($response->mailupBody->ReturnCode == '-1007') {
          throw new MailUpException("Webservices may be disabled, check your console configuration.");
        } else if ($response->mailupBody->ReturnCode == '-1011') {
          throw new MailUpException("IP address may not be properly configured, check your console configuration.");
        } else if ($response->mailupBody->ReturnCode == '-1001') {
          throw new MailUpException("Authentication error.");
        } else if ($response->mailupBody->ReturnCode == '-1002') {
          throw new MailUpException("Authentication error.");
        }
        return $response;
      }
      $logs = array();

      foreach ($errors as $error) {
        $logs[] = "{$error->line}({$error->column}): {$error->message}({$error->code})";
      }
      
      $logs = implode(';', $logs);
      throw new MailUpException("Can't parse server response.$logs");
    }
  }
  
} 
?>