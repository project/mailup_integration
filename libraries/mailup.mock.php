<?php
class MockMailUpClient {
  protected $wsImport;
  protected $wsSend;
  protected $configuration;

  /**
   * Library setup and initialization
   */     
  function configure ($configuration, $raiseException = false) {
    if ($raiseException)
      throw new MailUpException('Cannot initialize remote client. Please check your configuration.');
  }
  
  /**
   *  Retrieves lists
   *  @return array
   *    Lists associative array.
   */     
  function getLists ($behaviour = 'return') {
    if ($behaviour == 'empty') {
      return array (
        'lists' => array(),
        'listsByGuid' => array(),
        'returnCode' => null,
        'groups' => array(),
      );
    }
    
    if ($behaviour == 'return') {
      $lists = array (
        array (
          'idList' => 1,
          'listName' => 'TestList_1',
          'listGUID' => 'listGuid1',
          'groups' => array(
            1 => array(
              'id' => 1,
              'name' => 'group 1 of list 1',
            ),
            2 => array(
              'id' => 2,
              'name' => 'group 2 of list 2',
            ),
          ),
        ),
        array (
          'idList' => 2,
          'listName' => 'TestList_2',
          'listGUID' => 'listGuid2',
          'groups' => array(
            1 => array(
              'id' => 1,
              'name' => 'group 1 of list 1',
            ),
            2 => array(
              'id' => 2,
              'name' => 'group 2 of list 2',
            ),
          ),
        ),
      );
      $resultLists = array();
      foreach ($lists as $list) {
        $current = array (
          'id' => (int)$list['idList'],
          'name' => (string)$list['listName'],
          'guid' => (string)$list['listGUID'],
        );

        $resultLists [$current['id']] = $current;
        $listsByGuid [$current['guid']] = $current;
      }

      return array (
        'lists' => $resultLists,
        'listsByGuid' => $listsByGuid,
        'returnCode' => $returnCode
      );
    }
  }
  
  function getListById ($id) {
    $lists = $this->getLists();
    $list = $lists['lists'][$id];
    
    return array (
      'list' => $list,
      'returnCode' => 0,
    );
  }
  
  function getListByGuid ($guid) {
    $lists = $this->getLists();
    $list = $lists['listsByGuid'][$guid];
    
    return array (
      'list' => $list,
      'returnCode' => 0,
    );
  }
  
  function createList ($name, $success = true, $empty = false) {
    if ($empty) {
      return array();
    }
    return array (
      'id' => 3,
      'guid' => 'testGuid3',
      'name' => $name,
      'success' => $success,
    );
  }
  
  function deleteList ($id) {
    $client = $this->wsSend;
    $parameters = array(
      'listId' => $id,
      'autoId'=>''
    );
    return true;    
    
  }
  
  function importUsers ($users,$lists,$params = array()) {
    if (count($users) == 0) 
      return false;
    
    $xml = $this->generateExportable($users,'xml');
    
    if ($xml == "")
      return false;
    
    if (count($lists) == 0)
      return false;
    
    $listIDs = array();
    $listGUIDs = array();
    foreach ($lists as $list) {
      $listIDs [] = $list['id'];
      $listGUIDs [] = $list['guid'];
    }
    
    if (count($listIDs) == 0 || count($listGUIDs) == 0)
      return false;
      
    $listIDs = implode(";",$listIDs);
    $listGUIDs = implode(";",$listGUIDs);
    
    $defaults = array (
      // seems mandatory
      "groupsIDs" => '1', 
      'importType' => '3',
      'mobileInputType' => '2',
      'asPending' => '',
      'ConfirmEmail' => '0',
      'asOptOut' => '0',
      'forceOptIn' => '0',
      'replaceGroups' => '0',
      'idConfirmNL' => '0',
    );

    $params['listsIDs'] = $listIDs;
    $params['listsGUIDs'] = $listGUIDs;
    $params['xmlDoc'] = $xml;
    
    $data = $params + $defaults;
    
    $client = $this->wsImport;
    $response = $client->invoke('StartImportProcesses',$data);
    
    if ($response == null)
      return null;
      
    return array (
      'success' => ((int)$response->mailupBody->processes->process->ReturnCode) == 0,
      'returnCode' => (int)$response->mailupBody->processes->process->ReturnCode,
      'processId' => (int)$response->mailupBody->processes->process->processID,
      'listID' => (int)$response->mailupBody->processes->process->listID,
    );
  }
  
  /**
   *  Generate export content. Each supported format is usable for MailUp console import.
   *  @param array $users
   *    The users list. Each user is a stdClass object defining at least
   *    $user->mail and $user->name properties.
   *  @param string $type
   *    Exportable format, can be 'xml' or 'csv'.
   *  @return string
   *    The export content.
   */              
  function generateExportable ($users, $type = 'xml') {
    $data = '';
    // We iterate through every user and add fields to the output string.
    foreach ($users as $account) {
      if ($type == 'xml') {
        $data .= "\t<subscriber email=\"{$account->mail}\" Name=\"{$account->name}\">";
        if (!empty($account->customFields)) {
          foreach ($account->customFields as $id => $value) {
            $data .= "\n\t\t<campo$id>$value</campo$id>";
          }
        }
        $data .= "\n\t</subscriber>\n";
      } else if ($type == 'csv') {
        $row = array();
        $row[] = "{$account->mail}";
        if (!empty($account->customFields)) {
          foreach ($account->customFields as $value) {
            $row[] = $value;
          }
        }
        $row = implode(";",$row);
        $data[] = $row;
      }
    }
    // Finalize basing on the output format.
    if ($type == 'xml') {
      $data = "<subscribers>\n$data</subscribers>";
    } else if ($type == 'csv') {
      $data = implode("\n",$data);
    }
    return $data;
  }

  /**
   *  @return array
   *    Each array item has 'id' and 'name' fields. An empty array returns
   *    if some error occurrs.
   */
  public function getProfileFields($empty = false) {
    if ($empty) {
      return array();
    }

    return array (
      array (
        'id' => 1,
        'name' => 'First Name',
      ),
      array (
        'id' => 2,
        'name' => 'Last Name',
      ),
      array (
        'id' => 3,
        'name' => 'Company',
      ),
      array (
        'id' => 4,
        'name' => 'Address',
      ),
    );
  }

  function getGroup ($list) {
  
  }
  
  function subscribeUser ($user) {
  
  }
  
  function unsubscribeUser ($user) {
  
  }
  
  // Frontend Mock
  function invoke($method, $parameters, $behaviour = 'success') {
    if ($behaviour == 'success') {
      return array (
        'success' => TRUE,
        'response' => 0,
        'querystring' => 'QUERY',
      );
    } else if ($behaviour == 'throw') {
      throw new MailUpException('Request failed');
    } if ($behaviour == 'fail') {
      return array (
        'success' => FALSE,
        'response' => 1,
        'querystring' => 'QUERY',
      );
    }
  }
}
?>