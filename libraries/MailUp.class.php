<?php
/**
 * MailUp remote services wrapper
 */ 
abstract class MailUp {
  protected $wsdl;
	protected $initialized;
	protected $client;
	protected $configuration;
	
	/**
   * Constructor
   */     
	function __construct() {
		$this->initialized = false;
	}

	/**
	 * Destructor
	 */   	
	function __destruct() {
		unset($this->client); 
	}
	
  /**
   *  Sets client configuration.
   *  @param array $configuraton
   *    The configuration array
   *  @return boolean
   *    True on initialization success, false otherwise
   */
	function setConfiguration($configuration) {
    $this->configuration = $configuration;
    return $this->initialize();
  }
  
  /**
   * Initialize hook
   */     
  protected abstract function initialize ();
	
	/**
	 * Wrapper to remote methods, invoke remote method using SOAP protocol
	 * @param string $method
	 *   The target method
	 * @param array $parameters
	 *   Parameters array
	 * @param boolean $raw
	 *   Shall this function invoke the postInvoke hook using
	 *   raw SOAP response
	 * @return
	 *   SimpleXMLElement response tree
	 */   	
	function invoke($method, $parameters = null, $raw = false) {
	  if (!$this->initialized)
	   throw new MailUpException('Not initialized.');
	  
	  // Invoke pre elaboration hook
	  $this->preInvoke ($method, $parameters, $raw);
	
	  if ($parameters == null) {
      $response = $this->client->$method();
    } else {
      $response = $this->client->$method($parameters);
    }
    
    if (!$raw) {
      $response = $this->client->__getLastResponse();
    } 
        
    // Invoke post elaboration hook
    //try  {
      $this->postInvoke($response,$method,$parameters, $raw);
    //} catch (Exception $e) {
    //  return null;
    //}
    return $response;
  }
  
  /**
   * Pre invoke hook
   */     
  protected function preInvoke (&$method,&$parameters = null, &$raw = false) {}
  
  /**
   * Post invoke hook
   */     
  protected function postInvoke (&$response,$method,$parameters = null, $raw = false) {}
}
?>