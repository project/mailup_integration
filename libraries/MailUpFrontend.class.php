<?php
// @todo refactor & extend MailUp object
class MailUpFrontend {
  protected $endpoint;
  
  function configure($parameters) {
    $this->endpoint = $parameters['mailup_api_endpoint'];
  } 
  
  function invoke($method,$parameters) {
    $method = "{$this->endpoint}/{$method}.aspx";
    $query = $this->httpQuerystring($parameters);
    $response = $this->curlSend($method,$query);
    
    $result = array (
      'success' => $response !== false,
      'response' => $response,
      'querystring' => $query
    );
    
    return $result;
  }
  
  protected function curlSend($url, $querystring, array $options = array()) { 
    $defaults = array( 
        CURLOPT_POST => 1, 
        CURLOPT_HEADER => 0, 
        CURLOPT_URL => $url, 
        CURLOPT_FRESH_CONNECT => 1, 
        CURLOPT_RETURNTRANSFER => 1, 
        CURLOPT_FORBID_REUSE => 1, 
        CURLOPT_TIMEOUT => 4, 
        CURLOPT_POSTFIELDS => $querystring
    ); 
    
    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    
    // @todo better error handling
    if( ! $result = curl_exec($ch)) 
    { 
        $err = curl_error($ch);
        return false;
    } 
    curl_close($ch); 
    return $result; 
  } 
  
  protected function httpQuerystring($parameters) {
    $querystring = "";
    foreach ($parameters as $k=>$v) {
      $querystring [] = "$k=$v";
    }
    $querystring = implode("&",$querystring);
    
    return $querystring;
  }
  
}
?>