<?php

/**
 * Client class for MailSend Webservice
 */
class MailUpSend extends MailUp {
  protected $accessKey;
  
  /**
   * Constructor
   */     
	function __construct() {
	  $this->initialized = false;
		$this->accessKey = null;
	}
	
	function initialize() {
	  if (empty($this->configuration['mailup_ws_send_wsdl']))
	   return false;
	   
	  $this->client = new SoapClient($this->configuration['mailup_ws_send_wsdl'], array("trace" => 1, "exceptions" => 1));
    $this->getAccessKey();
    return $this->initialized = true;
  }
	
  protected function getAccessKey () {
    $parameters = array (
      'user' => $this->configuration['mailup_ws_import_user'],
      'pwd' => $this->configuration['mailup_ws_import_pass'],
      'consoleId' => $this->configuration['mailup_console_id'],
    );
    $xml = $this->client->LoginFromId($parameters)->LoginFromIdResult;
    $xml = simplexml_load_string($xml,'SimpleXMLElement', LIBXML_NOCDATA);
    // @see https://bugs.php.net/bug.php?id=29500
    $this->accessKey = (string)$xml->accessKey;
  }
  
  protected function preInvoke (&$method,&$parameters = null, &$raw = false) {
    $parameters['accessKey'] = $this->accessKey;
    // @todo check, high-coupling postInvoke
    $raw = true;
  }
  
  /**
   * Post invoke hook
   * @todo check if is this method enough general purpose, or move logic to mailup.rename
   */     
  protected function postInvoke (&$response,$method,$parameters = null, $raw = false) {
    $key = $method . "Result";
    $response = $response->$key;
    $response = simplexml_load_string($response);
  }
  
}
?>