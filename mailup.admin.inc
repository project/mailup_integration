<?php
/**
 * @file
 * This module provides administration settings.
 */

/**
 *  Form builder for admin/settings/mailup.
 *  @param array $form_state
 *    Drupal $form_state. 
 *  @return array
 *    The form. If $form_state['storage']['confirm'] is not NULL and properly
 *    set, it will return a confirm form. Otherwise the standard settings form
 *    will be returned.
 */
function mailup_settings_form($form_state = NULL) {
  // Storage/confirm is not empty, so the action will require user confirm.
  if(!empty($form_state['storage']['confirm'])) {
    $form = mailup_settings_form_confirm($form_state);
    return confirm_form($form, 'sicuro?', '/admin/settings/mailup', t('You have changed parameters that may involve a console switch.
Local data, such as profile mapping or list name aliases, will be lost. This action cannot be undone. Are you sure?'), t('Yes, switch console ID.'), t('Cancel'));
  }
  // Let's build form array.
  $form = array();
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('MailUp account configuration'),
    '#description' => t('Those are needed in order to enable webservices access.'),
    '#tree' => false,
  );
  $form['account']['mailup_console_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Console Id'),
    '#default_value' => variable_get('mailup_console_id', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t('Usually webservices-access username, less the first alphabetic character.'),
    '#required' => true,
  );
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Configuration'),
    '#tree' => false,
  );
  $form['api']['mailup_api_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#default_value' => variable_get('mailup_api_endpoint', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("Endpoint to API Xml* services."),
    '#required' => true,
  );
  $form['ws_import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webservice MailUpWsImport Configuration'),
    '#tree' => false,
  );
  $form['ws_import']['mailup_ws_import_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WebService MailUpWsImport WSDL'),
    '#default_value' => variable_get('mailup_ws_import_wsdl', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("WebService MailUpWsImport WSDL"),
    '#required' => true,
  );
  $form['ws_import']['mailup_ws_namespace'] = array(
    '#type' => 'textfield',
    '#title' => t('Namespace'),
    '#default_value' => variable_get('mailup_ws_namespace', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => '',
    '#required' => true,
  );
  $form['ws_import']['mailup_ws_import_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Webservices Username'),
    '#default_value' => variable_get('mailup_ws_import_user', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t('Webservices Username. You can get this value from your MailUp console, in the WebServices section.'),
    '#required' => true,
  );
  $form['ws_import']['mailup_ws_import_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Webservices Password'),
    '#default_value' => variable_get('mailup_ws_import_pass', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t('Webservices Password. You should configure this in your MailUp console, in the WebServices section.'),
    '#required' => true,
  );
  $form['ws_send'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webservice MailUpSend Configuration.'),
    '#tree' => false,
  );
  $form['ws_send']['mailup_ws_send_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WebService MailUpSend WSDL'),
    '#default_value' => variable_get('mailup_ws_send_wsdl', ''),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("Webservice MailUpSend WSDL"),
    '#required' => true,
  );
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('User registration subscription'),
    '#tree' => false,
  );
  $form['list']['mailup_on_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow new users to subscribe the list within the registration form'),
    '#default_value' => variable_get('mailup_on_register', 1),
    '#description' => t("If checked, a subscription flag will show in the registration form."),
    '#required' => FALSE,
  );
  $form['confirm'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email confirmation'),
    '#tree' => false,
  );
  $form['confirm']['mailup_confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use email confirm service'),
    '#default_value' => variable_get('mailup_confirm', 0),
    '#description' => t("If checked, each time a user performs the subscription a confirmation email is sent."),
    '#required' => FALSE,
  );
  $form['disclaimer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Terms of service'),
    '#tree' => false,
  );
  $disclaimer = file_get_contents(dirname(__FILE__) . "/terms.txt");
  $form['disclaimer']['mailup_disclaimer_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Terms of service'),
    '#default_value' => variable_get('mailup_disclaimer_text', $disclaimer),
    '#description' => t("This text is shown in the user registration form, user has to accept this terms to proceed."),
    '#required' => TRUE,
  );
  $form['submit'] = array (
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 *  Form submit handler for admin/settings/mailup.
 *  If the form has not been confirmed in the validator function it will be
 *  rebuild and a confirm form will be rendered, otherwise the form will be
 *  processed.
 *  @param array $form
 *    Drupal $form.
 *  @param array $form_state
 *    Drupal $form_state.
 */
function mailup_settings_form_submit($form, &$form_state) {
  if (empty($form_state['storage']['confirm'])) {
    // Form has not been confirmed, set for rebuild.
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
    return;
  } else {
    // Form has been confirmed, so we can store the updated information and
    // eventually cleanup local data.
    if ($form_state['storage']['cleanup']) {
      mailup_cleanup();
    }
    foreach ($form_state['values'] as $key => $value) {
      variable_set($key, $value);
    }
    $form_state['rebuild'] = FALSE;
    $form_state['storage']['confirm'] = FALSE;
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 *  Form validation handler for admin/settings/mailup.
 *  Flags form as 'confirmed' if console id has not been modified.
 *  @param array $form
 *    Drupal $form.
 *  @param array $form_state
 *    Drupal $form_state.
 */
function mailup_settings_form_validate($form, &$form_state) {
  // If confirm is not empty we already should have passed the confirm steps,
  // so no action is required.
  if (empty($form_state['storage']['confirm'])) {
    // Any change to any of those fields means a console switch.
    $fields = array (
      'mailup_console_id',
      'mailup_api_endpoint',
      'mailup_ws_import_wsdl',
      'mailup_ws_import_user',
    );
    $console_changed = FALSE;
    foreach ($fields as $field) {
      if (variable_get($field, -1) != $form_state['values'][$field]) {
        $console_changed = TRUE;
        break;
      }
    }
    if (!$console_changed) {
      // Console has not changed, we can confirm form submission.
      $form_state['storage']['confirm'] = TRUE;
      $form_state['storage']['cleanup'] = FALSE;
    } else {
      $form_state['storage']['cleanup'] = TRUE;
    }
  }
}

/**
 *  Form confirm builder for admin/settings/mailup.
 *  It's triggered when a console id change occurrs.
 *  @param array $form_state
 *    Drupal $form_state.
 *  @return array
 *    The confirm form, with previously submitted values as hidden fields.
 */
function mailup_settings_form_confirm($form_state) {
  $form = array();
  foreach ($form_state['values'] as $key => $value) {
    $form[$key]['#type'] = 'hidden';
    $form[$key]['#default_value'] = $value;
  }
  $form['#submit'] = array('mailup_settings_form_submit');
  return $form;
}

function mailup_admin_list_form($form_state, $list = NULL) {
  $form = array();
  $weight = 2;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => ($list != NULL) ? $list['name'] : '',
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("List name."),
    '#required' => true,
    '#weight' => $weight++,
  );
  if ($list != NULL) {
    $form['name']['#attributes'] = array('readonly' => 'readonly');
  }

  $form['local_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Local Name'),
    '#default_value' => ($list != NULL) ? $list['local_name'] : '',
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("You can specify an alternative, user-friendly name that is used for end-user display purpose."),
    '#required' => FALSE,
    '#weight' => $weight++,
  );
  
  if ($list != NULL) {
    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('Id'),
      '#default_value' => $list['id'],
      '#size' => 6,
      '#maxlength' => 6,
      '#attributes' => array('readonly' => 'readonly'),
      '#description' => t("List ID."),
      '#weight' => 0,
    );
    
    $form['guid'] = array(
      '#type' => 'textfield',
      '#title' => t('GUID'),
      '#default_value' => $list['guid'],
      '#size' => 64,
      '#maxlength' => 64,
      '#disabled' => TRUE,
      '#description' => t("List GUID."),
      '#weight' => 1,
    );
  }
  $form['#redirect'] = 'admin/settings/mailup/list/configuration';
  $form['mode'] = array('#type' => 'hidden',);
  if ($list == NULL) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => $weight++,
    );
    $form['mode']['#value'] = 'insert';
  } else {
    // List behaviour.
    $form['register'] = array(
      '#type' => 'fieldset',
      '#title' => t('Registration settings'),
      '#description' => t('You can show or hide this list from the registration form.'),
      '#tree' => FALSE,
      '#weight' => $weight++,
    );
    $form['register']['register_flag'] = array (
      '#type' => 'checkbox',
      '#default_value' => $list['register'],
      '#title' => t('Show this list in the registration form'),
      '#weight' => $weight++,
    );
    // Groups handling.
    $form['groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('Enable list groups'),
      '#description' => t('Those are the groups found for this list. Users that subscribe this list will be inserted into the flagged groups.'),
      '#tree' => FALSE,
      '#weight' => $weight++,
    );
    if (!empty($list['groups'])) {
      foreach ($list['groups'] as $group) {
        $form['groups']['group_' . $group['id']] = array (
          '#type' => 'checkbox',
          '#default_value' => $group['register'],
          '#title' => $group['name'],
          '#weight' => $weight++,
        );
      }
    } else {
      $form['groups']['#description'] = t('No groups found for this list.');
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#weight' => $weight++,
    );
    $form['mode']['#value'] = 'update';
  }
  return $form;
}

function mailup_admin_list_form_submit($form, &$form_state) {
  if ($form_state['values']['mode'] == 'insert') {
    $list = mailup_create_list($form_state['values']['name']);
    if (!empty($list)) {
      $list['local_name'] = $form_state['values']['local_name'];
      mailup_save_list($list);
      drupal_set_message(t('The new list has been created with id @id. Its GUID: @guid.',array('@id'=>$list['id'],'@guid'=>$list['guid'])));
    } else {
      drupal_set_message(t('Unable to create list'), 'error');
    }
  } else if ($form_state['values']['mode'] == 'update') {
    $list['id'] = $form_state['values']['id'];
    $list['local_name'] = $form_state['values']['local_name'];
    $local_list = mailup_local_list($list['id']);
    $list['groups'] = $local_list['groups'];
    $list['register'] = $form_state['values']['register_flag'];

    foreach ($form_state['values'] as $key => $value) {
      if (strpos($key, 'group_') === FALSE) {
        continue;
      }
      $chunks = explode('_', $key);
      $gid = $chunks[1];
      $list['groups'][$gid]['register'] = $value;
    }
    mailup_save_list($list);
    drupal_set_message(t('List has been updated.'));
  }
}

function mailup_admin_list_configuration() {
  $lists = mailup_get_lists();
  if (empty($lists)) {
    drupal_set_message(t('No list to display.'));
    return "";
  }

  $default_list = variable_get('mailup_default_list',NULL);
  $found = FALSE;
  $rows = array();
  foreach ($lists as $list) {
    $row = array();
    $row [] = $list['id'];
    $row [] = l(empty($list['local_name'])?$list['name']:$list['local_name'],"admin/settings/mailup/list/{$list['id']}/edit");
    $row [] = $list['guid'];
    if ($list['enabled']) {
      $found = TRUE;
      $row [] = t('Enabled list') . ' (' . l(t('disable'),"admin/settings/mailup/list/{$list['id']}/enable/0") . ')';
    } else {
      $row [] = l(t('Enable this list'),"admin/settings/mailup/list/{$list['id']}/enable");
    }
    $rows [] = $row;
  }

  $header = array (
    t('Id'),
    t('Name'),
    t('GUID'),
    t('Action')
  );

  if (!$found) {
    drupal_set_message(t("You didn't enable any list. Drupal cannot synchronize with MailUp unless you do this."),'warning');
  }
  return theme_table($header,$rows);
}

function mailup_admin_users_import () {
  return drupal_get_form('mailup_users_import_groups_form');
}

/**
 * Menu callback
 */ 
function mailup_admin_users_export () {
  return drupal_get_form('mailup_export_users_form');
}

/**
 * Setup filters for users export
 */ 
function mailup_export_users_form () {

  $form = array();
  
  $form["type"] = array(
    '#type' => 'radios',
    '#title' => t('Format'),
    '#default_value' => 'csv',
    '#options' => array('xml'=>'Xml','csv'=>'CSV'),
  );
  
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Choose roles'),
    '#description' => t('Leave unchecked to export every role'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#tree' => false,
  );
  
  $roles = user_roles(true);
  
  foreach ($roles as $rid=>$role) {
    
    $form['filter']["role_{$rid}"] = array(
      '#type' => 'checkbox',
      '#title' => $role,
      '#default_value' => 0,
      
    );
  }
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Download'));
  
  return $form;
}

/**
 * Users export submit handler
 */ 
function mailup_export_users_form_submit ($form, &$form_state) {
  
  // Get role filters
  $roles = array();
  foreach ($form_state['values'] as $k=>$v) {
    $chunks = explode("_",$k);
    if (count($chunks) > 1) {
      if ($chunks[0] == "role" && $v)
        $roles [] = $chunks[1];
    }
  }

  // File content
  $content = mailup_generate_exportable($form_state['values']['type'],array('roles'=>$roles));
  if ($content === false) {
    return;
  }
  
  // Setup file
  $filename = "mailup_users_export.{$form_state['values']['type']}";
  $path = file_create_filename($filename,file_directory_temp());
  $ret = file_save_data($content,$path);
  
  if ($ret === 0) {
    drupal_set_message(t('Cannot create file @file', array('@file' => $path)),'error');
    return;
  }
  
  // Setup headers for download
  $headers = array(
    "Content-Type: text/xml",
    "Content-Disposition: attachment; filename=".$filename,
    "Content-Length: ".filesize($path),
  );
  
  // Send http data
  file_transfer($path,$headers);
}

/**
 * Menu callback
 */
function mailup_admin_enable_list($list) {
  if ($mode = arg(6) == NULL) {
    db_query("update {mailup_list} set enabled=1 where id=%d", $list['id']);
    drupal_set_message(t("<strong>!name</strong> list has been added as a choice for website newsletters.",array('!name' => $list['name'])));
  } else {
    db_query("update {mailup_list} set enabled=0 where id=%d", $list['id']);
    drupal_set_message(t("<strong>!name</strong> list has been removed as a choice for website newsletters.",array('!name' => $list['name'])));
  }
  drupal_goto('admin/settings/mailup/list/configuration');
}

/**
 *  Users import entry point. Admins will use this form to select
 *  which user pick for the import, and to select the target list/groups aswell.
 */
function mailup_users_import_groups_form($form_id) {
  if (variable_get('mailup_cron_start', 0)) {
    drupal_set_message(t('An import process is already scheduled and will be canceled if you start a new one.'), 'warning');
  }
  $roles = user_roles();
  $lists = mailup_get_lists();

  $form['lists'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lists'),
    '#description' => t("Select list and groups as the import target."),
    '#tree' => false,
  );

  foreach($lists as $list) {
    $form['lists']['list_' . $list['id']] = array (
      '#type' => 'checkbox',
      '#title' => $list['local_name']?$list['local_name']:$list['name'],
      '#default_value' => $list['enabled'],
    );
    $form['lists']['groups' . $list['id']] = array(
      '#type' => 'fieldset',
      '#title' => t('Groups'),
      '#prefix' => "<div style='padding-left:16px'>",
      '#suffix' => "</div>",
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => FALSE,
    );
    if (empty($list['groups'])) {
      $form['lists']['groups' . $list['id']] = array (
          '#prefix' => "<div style='font-size:85%'>",
          '#suffix' => "</div>",
          '#value' => t('This list has no groups.'),
        );
    } else {
      foreach ($list['groups'] as $group) {
        $form['lists']['groups' . $list['id']]['group_' . $list['id'] . '_' . $group['id']] = array (
          '#type' => 'checkbox',
          '#title' => $group['name'],
          '#default_value' => $group['register'],
        );
      }
    }
  }

  $form['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t("Selected roles you want to be imported. If you don't select any, every user will be imported."),
    '#tree' => false,
  );

  foreach ($roles as $k => $v) {
    // @fixme hardcoded value for anonymous group
    if ($k == 1) continue;
    $form['roles']['role_' . $k] = array(
      '#type' => 'checkbox',
      '#title' => $v,
      '#default_value' => 0,
      '#required' => false,
    );
  }

  $form['#redirect'] = 'admin/settings/mailup/list/configuration';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 *  Validation handler. 
 */
function mailup_users_import_groups_form_validate($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    $chunks = explode('_', $key);
    if ($chunks[0] == 'list') {
      if (!isset($postedLists[$chunks[1]]) && $value) {
        $postedLists[$chunks[1]] = array();
      }
    }
  }
  if (empty($postedLists)) {
    form_set_error('', t('You did not select any list.'));
    return;
  }
}

/**
 *  Submit handler. It will parse posted data and save it in a session
 *  variable, to make it available in the import review page.
 */
function mailup_users_import_groups_form_submit($form, &$form_state) {
  // In every case we no longer need the stored data, so clean it up.
  mailup_clean_schedule();
  // Detect requested roles.
  foreach($form_state['values'] as $key => $val) {
    if (strpos($key, 'role') === FALSE) {
      continue;
    }
    $chunks = explode('_',$key);
    if (count($chunks) != 2) {
      continue;
    }
    if ($val) {
      $roles[] = $chunks[1];
    }
  }
  // If roles is set, we're applying criteria. Else just fetch every user.
  if (empty($roles)) {
    $result = db_query("select * from {users}");
  } else {
    $placeholders = implode(', ', array_fill(0, count($roles), "%d"));
    $result = db_query("select * from {users} u inner join {users_roles} ur on u.uid=ur.uid where ur.rid in ($placeholders)",$roles);
  }
  // Compose users array.
  $users = array();
  while ($account = db_fetch_object($result)) {
    if ($account->uid <= 1) {
      continue;
    }
    $users [] = $account;
  }

  // Detect lists and groups now. 2 iterations for better readability.
  foreach ($form_state['values'] as $key => $value) {
    $chunks = explode('_', $key);
    if ($chunks[0] == 'list') {
      if (!isset($postedLists[$chunks[1]]) && $value) {
        $postedLists[$chunks[1]] = array();
      }
    } 
  }
  foreach ($form_state['values'] as $key => $value) {
    $chunks = explode('_', $key);
    if ($chunks[0] == 'group' && array_key_exists($chunks[1], $postedLists) && $value) {
      $postedLists[$chunks[1]][$chunks[2]] = $chunks[2];
    }
  }
  if (empty($postedLists)) {
    drupal_set_message(t('You did not select any list.'), 'warning');
  } else {
    // Build the list array.
    foreach ($postedLists as $list_id => $list) {
      $temp_list = mailup_local_list($list_id);
      foreach ($temp_list['groups'] as $group_id => &$group) {
        if (!array_key_exists($group_id, $list)) {
          unset($temp_list['groups'][$group_id]);
        }
      }
      $lists[] = $temp_list;
    }
    mailup_store_import_data($lists, $users, $roles);
    drupal_get_messages();
    drupal_goto('admin/settings/mailup/users/import/review');
    return;
  }
}

/**
 *  Menu callback for admin/settings/mailup/users/import/review.
 */
function mailup_users_import_review() {
  // Get data from session.
  $data = mailup_get_import_data();
  // If we have no data there are 2 cases:
  //  - admin landed here after he scheduled an import process
  //  - admin landed here for no reason, so there's nothing to show him
  if (empty($data)) {
    if (variable_get('mailup_cron_start', 0)) {
      drupal_set_message(t('An import process is already scheduled.'), 'warning');
    } else {
      drupal_set_message(t('There are no users to import.'), 'warning');
    }
    return "";
  }
  // We have fresh data, so an import configuration has been submitted.
  // We check elapsed time since last submit and cleanup data if the threshold
  // has been exceed. The threshold is expressed in seconds.
  $time_threshold = variable_get('mailup_import_threshold', 180);
  if (time() - $data['timestamp'] > $time_threshold) {
    mailup_clean_schedule();
    drupal_set_message(t('You have configured an import process more than @minutes minutes ago and this old data has been deleted for security reasons. Please setup a new import process !here.', array('@minutes' => round($time_threshold/60), '!here' => l(t('here'), 'admin/settings/mailup/users/import'))), 'warning');
    return "";
  }
  $users = count($data['users']);
  $threshold = 1000;
  if ($users > $threshold) {
    drupal_set_message(t('Since you are importing over @threshold users you would maybe consider running a scheduled process.', array('@threshold' => $threshold)), 'warning');
  }
  return theme('mailup_import_review', drupal_get_form('mailup_users_import_run_form'), $data);
}

/**
 *  Import trigger form. It will allow admins to choose import mode and parameters.
 */
function mailup_users_import_run_form($form_id) {
  $form = array();
  $form['action'] = array(
    '#type' => 'fieldset',
    '#title' => t('Choose an action'),
    '#description' => t('Select one action for your selection. You can either subscribe or unsubscribe them.'),
    '#tree' => FALSE,
  );
  $form['action']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Action'),
    '#description' => '',
    '#options' => array(0 => t('Subscribe'), 1 => t('Unsubscribe')),
    '#required' => TRUE,
  );
  $form['confirm'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email confirmation'),
    '#description' => '',
    '#tree' => FALSE,
  );
  $form['confirm']['mailup_confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use email confirm service'),
    '#default_value' => FALSE,
    '#description' => '',
    '#required' => FALSE,
  );
  $form['run'] = array(
    '#type' => 'fieldset',
    '#title' => t('Run now'),
    '#description' => t('Use this if you want to import instantly every user in your selection.'),
    '#tree' => FALSE,
  );
  $form['schedule'] = array(
    '#type' => 'fieldset',
    '#title' => t('Setup a scheduled synchronization'),
    '#description' => t('Use this if you want the import process to be scheduled and split on several cron runs.'),
    '#tree' => FALSE,
  );
  $form['schedule']['users'] = array(
    '#type' => 'select',
    '#title' => t('Users'),
    '#description' => t('Select how many users you wish to import in each scheduled step.'),
    '#options' => array(100 => 100, 500 => 500, 1000 => 1000, 2000 => 2000, 3000 => 3000, 5000 => 5000),
    '#tree' => FALSE,
  );
  $form['run']['submit'] = array('#type' => 'submit', '#value' => t('Run'));
  $form['schedule']['submit'] = array('#type' => 'submit', '#value' => t('Schedule'));
  return $form;
}

/**
 *  Submit handler. It will trigger the import process or save required data
 *  to trigger a scheduled import.
 */
function mailup_users_import_run_form_submit($form_id, &$form_state) {
  // Read and clean up session and schedule data.
  $data = mailup_get_import_data();
  mailup_clean_schedule();
  $users = $data['users'];
  $lists = $data['lists'];
  // Read action
  $action = $form_state['values']['type'];
  // Detect import mode.
  if ($form_state['values']['op'] == t('Run')) {
    // Trigger the import process with auto-start.
    $response = mailup_import_users($users, $lists, $action, $form_state['values']['mailup_confirm']);
    if ($response == NULL) {
      drupal_set_message(t('Remote services gave an unexpected response, the import process may have been canceled.'), 'warning');
      drupal_goto('admin/settings/mailup/users/import');
    }
    // Parse the response
    $result = $response['success'];
    $process = $response['processId'];
    if (!$response['success']) {
      drupal_set_message(t('Import process failed with code: @err', array('@err' => $response['returnCode'])), 'error');
    } else {
      foreach ($lists as $list) {
        $list_names[] = $list['local_name'] ? $list['local_name'] : $list['name'];
      }
      $status_parameters = array(
        '@id' => $process,
        '@count' => count($users),
        '@lists' => implode(', ', $list_names),
      );
      $status_message = t('The import process is now running (id: @id). @count users will be imported in these lists: @lists', $status_parameters);
      drupal_set_message($status_message);
    }
    drupal_goto('admin/settings/mailup/users/import');
  } else if ($form_state['values']['op'] == t('Schedule')) {
    variable_set('mailup_cron_start', time());
    variable_set('mailup_cron_lists', $lists);
    variable_set('mailup_cron_limit', $form_state['values']['users']);
    variable_set('mailup_cron_offset', 0);
    variable_set('mailup_cron_action', $action);
    variable_set('mailup_cron_confirm', $form_state['values']['mailup_confirm']);
    foreach ($users as $account) {
      db_query("insert into {mailup_import_users} (uid) values (%d)", $account->uid);
    }
    drupal_set_message(t('The import process as been setup and will start on next cron.'));
    drupal_goto('admin');
  }
}

/**
 *  Profile mapping form.
 *  Menu callback for mailup/settings/profiles
 */
function mailup_admin_profiles_form() {
  // We build options from stored categories/profile_field
  $categories = profile_categories();
  if (empty($categories)) {
    drupal_set_message(t('There are not profile fields to map.'),'warning');
    return '';
  }
  $category_options = array();
  foreach ($categories as $category) {
    if ($category['name'] == 'MailUp') {
      continue;
    }
    // Fetch this category fields
    $result = _profile_get_fields($category['name']);
    if (empty($result)) {
      continue;
    }
    // Build the fields array
    while ($row = db_fetch_object($result)) {
      $fields[] = $row;
    }
    // Default options is for view purpose only
    $options = array();
    $options[0] = t("Don't map this field");
    $options[-1] = $category['name'];
    foreach ($fields as $field) {
      $options[$field->fid] = ' - ' . $field->title;
    }
    $category_options[] = $options;
  }
  // Get MailUp fields
  $mailup_fields = mailup_profile_fields();
  // Initialize form
  $form = array();
  $form['profiles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile mapping'),
    '#description' => t('You can map MailUp user fields to Drupal profile fields. Choose each drupal field to map from the dropdown list.'),
  );
  // Foreach static field we provide a select-option for the mapping.
  $idx = 0;
  foreach ($mailup_fields as $key => $mailup_field) {
    foreach ($category_options as $options) {
      $form['profiles'][$key] = array (
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $mailup_field['fid'],
        '#title' => $mailup_field['display'],
        '#weight' => $idx++,
      );
    }
  }
  $form['profiles']['submit'] = array (
    '#type' => 'submit',
    '#value' => 'Save',
    '#weight' => $idx,
  );
  return $form;
}

/**
 *  Validate handler for mailup_admin_profiles_form
 */
function mailup_admin_profiles_form_validate($form, &$form_state) {
  // Retrieve fields
  $mailup_fields = mailup_profile_fields();
  $update = array();
  foreach ($form_state['values'] as $key => $value) {
    if (array_key_exists($key, $mailup_fields)) {
      if ($value == 0 || $value == -1) {
        // Skip those input which aren't found in the mailup_fields array
        continue;
      }
      // Store each fid for processing. At the end of this,
      // $update should look like
      // array[fid_1] = form_field_name
      // array[fid_2] = form_field_name
      // etc
      $update[$value][] = $key;
    }
  }
  foreach ($update as $k => $v) {
    if (count($v) > 1) {
      // If $v counts more than 1 element, there is likely a mismatch.
      // Each of the $v elements is the form_field_name, a.k.a. the key
      // for retrieving the element in the $mailup_fields array.
      foreach ($v as $error) {
        $error_fields[] = $mailup_fields[$error]['display'];
      }
      form_set_error($k,t("You can't assign more than one field to the same profile field. Check settings for @field", array('@field' => implode(',',$error_fields))));
    }
  }
}

/**
 *  Submit handler for mailup_admin_profiles_form 
 */
function mailup_admin_profiles_form_submit($form, &$form_state) {
  $mailup_fields = mailup_profile_fields();
  $success = TRUE;
  foreach ($form_state['values'] as $key => $value) {
    // Skip those input which aren't found in the mailup_fields array
    if (array_key_exists($key, $mailup_fields)) {
      if ($value == -1) {
        // This is the category read-only option.
        continue;
      } else if ($value == 0) {
        // 0 is the command for delete mapping.
        $result = db_query("delete from {mailup_profiles} where mailup_field='%s'", $key);
      } else {
        $result = db_query("insert into {mailup_profiles} (fid,mailup_field) values (%d,'%s') on duplicate key update mailup_field='%s'", $value, $key, $key);
      }
      $success = $success && $result;
    }
  }
  if ($success) {
    drupal_set_message(t('Profile mapping has been updated.'));
  }
}
