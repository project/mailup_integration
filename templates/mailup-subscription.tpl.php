<div class='mailup-subscription'>
  <p>
    <?php print t('Your active subscriptions are listed below, you can activate and deactivate them from this page.') ?>
  </p>
  <?php
    foreach ($lists as $list) {
      if ($list['checked']) {
        $active[] = l(t('Unsubscribe @list', array('@list' => $list['local_name']?$list['local_name']:$list['name'],)), $list['action']);
      } else {
        $inactive[] = l(t('Subscribe @list', array('@list' => $list['local_name']?$list['local_name']:$list['name'],)), $list['action']);
      }
    }
    ?>
    <?php if (!empty($inactive)): ?>
      <h3><?php print t('Available newsletters')?></h2>
      <?php print theme('item_list', $inactive);?>
    <?php endif;?>

    <?php if (!empty($active)): ?>
      <h3><?php print t('Your active subscriptions')?></h2>
      <?php print theme('item_list', $active);?>
    <?php endif;?>
</div>
