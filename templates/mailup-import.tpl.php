<div class='mailup-import'>
  <?php if (count($data['roles'])): ?>
    <p><?php print t('You chose to import users from @roles groups in @lists MailUp lists.', array('@roles' => count($data['roles']), '@lists' => count($data['lists'])))?></p>
  <?php else: ?>
    <p><?php print t('You chose to import all of your users in @lists MailUp lists.', array('@lists' => count($data['lists'])))?></p>
  <?php endif; ?>
</div>
<?php print $form ?>