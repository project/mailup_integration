<?php
/**
 * Library includes.
 */ 
function mailup_initialize_library () {
  $includes = array (
    0=> array (
      'file' => '/libraries/mailup.php',
      'name' => 'Wrapper'
    ),
    1=> array (
      'file' => '/libraries/MailUpException.class.php',
      'name' => 'Exception'
    ),
    2=> array (
      'file' => '/libraries/MailUp.class.php',
      'name' => 'Library'
    ),
    3=> array (
      'file' => '/libraries/MailUpWsImport.class.php',
      'name' => 'Webservice Import'
    ),
    4=> array (
      'file' => '/libraries/MailUpSend.class.php',
      'name' => 'Webservice Send'
    ),
    5=> array (
      'file' => '/libraries/MailUpFrontend.class.php',
      'name' => 'Frontend'
    ),
    6=> array (
      'file' => '/libraries/mailup.mock.php',
      'name' => 'Wrapper'
    ),
  );

  $error = false;
  $count = count($includes);
  
  for ($i = 0; $i<$count; $i++) {
    $lib = $includes[$i];
    $path = drupal_get_path('module', 'mailup') . $lib['file'];
    if (!file_exists($path)) {
      watchdog('mailup', "Library not found: @lib", array('@lib' => $lib['file']), WATCHDOG_ERROR);
      drupal_set_message(t('Library not found: @lib',array('@lib' => $lib['name'])),'error');
      $error = true;
      continue;
    }
    require_once $path;
  }
}

function mailup_get_client() {
  // @fixme find any better mocking way.
  if (variable_get('mailup_unit_testing', 0)) {
    return new MockMailUpClient();
  }
  try {
    // We wrap initialization in a try-catch block, since the init process
    // is handling SOAP or remote requests that could fail.
    $mailup = new MailUpClient();
    $mailup->configure(mailup_remote_configuration());
  } catch (Exception $ex) {
    watchdog('mailup', "Can't initialize client. @ex", array('@ex' => $ex->getMessage()), WATCHDOG_ERROR);
  }
  // We return client, even if it's not been initialized. Init status is checked at
  // each invoke, so this should be safe enough.
  return $mailup;
}

/**
 *  Frontend wrapper.
 *  @param object $account
 *    The account being checked
 *  @param array $list
 *    The list associative array
 *  @return boolean
 *    true if $account i subscribed to $list, false otherwise
 */
function mailup_is_user_subscribed ($account, $list) {
  try {
    $mailup = new MailUpFrontend();
    $mailup->configure(mailup_remote_configuration());
    $response = $mailup->invoke('xmlchksubscriber', array('ListGuid'=>$list['guid'],'List'=>$list['id'],'Email'=>$account->mail));

    watchdog('mailup',"chksubscriber query: @query; response:@response", array('@query'=>$response['querystring'],'@response'=>$response['response']), WATCHDOG_NOTICE);

    if ($response['success'] === false) {
      watchdog('mailup', "Xml* curl failed while querying xmlChkSubscriber", array(), WATCHDOG_ERROR);
      return false;
    }

    if ($response['response'] == 2) {
      return true;
    }

    return false;
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'), 'error');
    return array();
  }
}

/**
 *
 *  Frontend wrapper for list subscription
 *  @param object $account
 *    The account being subscribed / unsubscribed.
 *  @param array $lists
 *    The list array, each an associative array itself. 
 *  @param string $mode
 *    If 'subscribe' $account is being subscribed, if 'unsubscribe' he's
 *    going to be unsubscribed.
 *  @return boolean
 *    TRUE on success, FALSE otherwise
 */
function mailup_subscribe($account, $lists = NULL, $mode = 'subscribe') {
  // Check the list
  if (empty($lists)) {
    watchdog('mailup', "No list specified for subscription.", array(), WATCHDOG_WARNING);
    return FALSE;
  }
  try {
    // Subscription handlers.
    $handler = array(
      'subscribe' => 'xmlSubscribe',
      'unsubscribe' => 'xmlunsubscribe',
    );
    // Get and configure the client.
    $mailup = new MailUpFrontend();
    $mailup->configure(mailup_remote_configuration());
    $use_confirm = variable_get('mailup_confirm', 0);
    $result = TRUE;
    // Perform subscription for each list.
    foreach ($lists as $list) {
      $parameters = _mailup_subscribe_parameters($list, $account, $use_confirm);
      $response = $mailup->invoke($handler[$mode], $parameters);
      watchdog('mailup', "Request sent for %user with code %code. Request query: %query", array('%user'=>$account->mail, '%code'=>$response['response'], '%query'=>$response['querystring']), WATCHDOG_NOTICE);

      // Response handling
      if(preg_match("/[a-zA-Z]+/",$response['response']))  {
        $result = $result && TRUE;
      } else {
        if ($response['response'] == 0) {
          $result = $result && TRUE;
        } else if ($response['response'] == 1 && $mode == 'subscribe') {
          watchdog('mailup', "%method to list %list failed due to generic error", array('%method' => $handler[$mode], '%list' => $list['id']), WATCHDOG_ERROR);
          $result = $result &&  FALSE;
        } else if ($response['response'] == 1 && $mode == 'unsubscribe') {
          // We are not altering $result here, user could never have subscribed this list
          // and this generates a '1' response code.
          watchdog('mailup', "%method to list %list failed due to generic error", array('%method' => $handler[$mode], '%list' => $list['id']), WATCHDOG_ERROR);
        } else if ($response['response'] == 2) {
          watchdog('mailup', "%method to list %list failed due to invalid email/phone", array('%method' => $handler[$mode], '%list' => $list['id']), WATCHDOG_ERROR);
          $result = $result &&  FALSE;
        } else if ($response['response'] == 3) {
          watchdog('mailup', "%method to list %list failed, user already subscribed", array('%method' => $handler[$mode], '%list' => $list['id']), WATCHDOG_NOTICE);
          $result = $result &&  TRUE;
        } else {
          watchdog('mailup', "%method to list %list failed, unkwown response: %response", array('%method' => $handler[$mode], '%list' => $list['id'], '%response' => $res,), WATCHDOG_ERROR);
          $result = $result &&  FALSE;
        }
      }
    }
    return $result;
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
    return FALSE;
  }
}

/**
 *  Returns parameters for Frontend subscribe call.
 *
 *  @param array $list
 *    The list user is being subscribed.
 *  @param object $account
 *    The user subscribing this list.
 *  @param boolean $use_confirm
 *    Whether to use confirm service or not.
 *  @return array
 *    The parameters associative array.
 */
function _mailup_subscribe_parameters($list, $account, $use_confirm = FALSE) {
  // Default parameters
  $groups = array();
  $parameters = array(
    'ListGuid' => $list['guid'],
    'List' => $list['id'],
    'email' => $account->mail,
    'Confirm' => $use_confirm,

  );
  // Handle list groups, if any
  if (!empty($list['groups'])) {
    foreach ($list['groups'] as $key => $value) {
      if ($value['register']) {
        $groups[] = $key;
      }
    }
    $groups = implode(',', $groups);
    $parameters['group'] = $groups;
  }
  // Update the user profile fields.
  $users = mailup_profile_adapt(array($account));
  $account = $users[0];
  if (!empty($account->customFields)) {
    foreach ($account->customFields as $field_id => $field) {
      // Skip empty fields.
      if (empty($field)) {
        continue;
      }
      // "campo" is MailUp reserved word for profile fields.
      $parameters['csvFldNames'][] = "campo$field_id";
      $parameters['csvFldValues'][] = $field;
    }
    if (!empty($parameters['csvFldNames'])) {
      $parameters['csvFldNames'] = implode(';', $parameters['csvFldNames']);
      $parameters['csvFldValues'] = implode(';', $parameters['csvFldValues']);
    }
  }
  return $parameters;
}

/**
 *  Frontend wrapper for xmlUnsubscribe, this is a shorthand for
 *  mailup_subscribe($account,$list,'unsubscribe').
 *
 *  @param object $account
 *    The account the operation is being performed on.
 *  @param array $lists
 *    Lists array to perform the unsubscribe on. If none is passed, the subscription
 *    is executed on every known list.
 *  @return boolean
 *    TRUE on success.
 */
function mailup_unsubscribe($account, $lists = NULL) {
  if ($lists == NULL) {
    $lists = mailup_local_lists();
  }
  return mailup_subscribe($account, $lists, 'unsubscribe');
}

function mailup_create_list ($name) {
  try {
    $mailup =  mailup_get_client();
    $response = $mailup->createList($name);
    if ($response['success']) {
      return $response;
    }
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
  }
  return array();
}

function mailup_delete_list($id) {
  $mailup = mailup_get_client();
  return $mailup->deleteList($id);

}

function mailup_get_list ($id) {
  try {
    $mailup = mailup_get_client();
    $response = $mailup->getListById($id);
    return $response['list'];
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
    return array();
  }
}

/**
 *  Query MailUp for available lists, then insert or update the local record.
 *  The return array comes from the local storage.
 *
 *  @return array
 *    MailUp lists.
 */ 
function mailup_get_lists () {
  try {  
    $mailup = mailup_get_client();
    $response = $mailup->getLists();
    if ($response['returnCode'] != 0) {
      drupal_set_message(t('Unexpected response from server: @code', array('@code' => $response['returnCode'])), 'error');
      return array();
    }
    foreach ($response['lists'] as $list) {
      mailup_save_list($list);
    }
    return mailup_local_lists();
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
    return array();
  }
  
}

/**
 *  @return string
 *    The file content or false if fail
 */ 
function mailup_generate_exportable ($type = 'xml', $filters = array()) {
  // Read the users
  if (isset($filters['roles']) && count($filters['roles']) > 0) {
    $query = "select * from users u inner join users_roles r on u.uid=r.uid where r.rid in (" . implode(',',$filters['roles']) . ") and u.uid!=0 and u.uid!=1";
  } else {
    $query = "select * from users where uid!=0 and uid!=1";
  }
  $result = db_query($query);
  
  // Generate the users array
  $users = array();
  while ($account = db_fetch_object($result)) {
    if ($account->uid == 0 || $account->uid == 1) continue;
    $users [] = $account;
  }
  
  if (empty($users)) {
    drupal_set_message(t('No user found. Remark: you cannot export the anonymous user or the superadmin.'));
    return false;
  }

  // Adapt profiles
  $users = mailup_profile_adapt($users);

  try {
    // Ask for the file content
    $mailup = mailup_get_client();
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
    return false;
  }
  // gg
  return $mailup->generateExportable($users, $type);
}

function mailup_remote_configuration () {
  return array (
    'mailup_console_id' => variable_get('mailup_console_id',''),
    'mailup_api_endpoint' => variable_get('mailup_api_endpoint',''),
    'mailup_ws_import_wsdl' => variable_get('mailup_ws_import_wsdl',''),
    'mailup_ws_import_user' => variable_get('mailup_ws_import_user',''),
    'mailup_ws_import_pass' => variable_get('mailup_ws_import_pass',''),
    'mailup_ws_namespace' => variable_get('mailup_ws_namespace',''),
    'mailup_ws_send_wsdl' => variable_get('mailup_ws_send_wsdl',''),
    'mailup_confirm' => variable_get('mailup_confirm', 0),
  );
}

/**
 *  Setup an import process with auto start.
 *
 *  @param array $users
 *    The users being imported.
 *  @param array $lists
 *    Target lists.
 *  @param int $action
 *    Subscribe or unsubscribe users. Use 0 for subscribe, 1 for unsubscribe.
 *  @param boolean $confirm
 *    Not used yet.
 *  @param boolean $start_process
 *    Not used yet.
 *  @return mixed
 *    @todo refactor library
 */ 
function mailup_import_users ($users, $lists, $action, $confirm = FALSE, $start_process = TRUE) {
  try {
    $params = array();
    $params['ConfirmEmail'] = $confirm;
    if ($action == 0) {
      $params['forceOptIn'] = '1';
    } else if ($action == 1) {
      $params['asOptOut'] = '1';
    }
    // Adapt profiles
    // @todo  Consider refactoring this. The mailup client generate_exportable
    //        works fine only on adapted users, this make almost useless a
    //        generate_exportable call without the previous mailup_profile_adapt.
    $users = mailup_profile_adapt($users);
    $mailup = mailup_get_client();
    return $mailup->importUsers($users, $lists, $params);
  } catch (MailUpException $exception) {
    $ex = $exception->getMessage();
    watchdog('mailup', "$ex", array(), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 *  Reads remote user fields.
 *  @return array
 *    MailUp fields we can map to profile fields. Each field is defined as
 *    - $field[mailup_field_identifier]['display'] = Text for GUI purpose
 *    - $field[mailup_field_identifier]['fid'] = profile field mapped, or 0 if
 *      no mapping exists
 *    - $field[mailup_field_identifier]['drupal_field'] = if mapped, the drupal-profile
 *      field name, to be used in some $account->field context.
 */
function mailup_profile_fields() {
  try {
    // Get remote fields
    $mailup = mailup_get_client();
    $fields = $mailup->getProfileFields();

    // Adapt remote fields to local settings
    $mailup_fields = array();
    foreach ($fields as $field) {
      // We are skipping the native email, since its mapping is automated.
      if (preg_match("/[Ee]mail/", $field['name'])) {
        continue;
      }
      $mailup_fields[$field['id']] = array (
        'display' => $field['name'],
        'fid' => 0,
      );
    }

    // Read local mapping
    $result = db_query("select m.fid, m.mailup_field, p.name from {mailup_profiles} m left join {profile_fields} p on m.fid=p.fid");
    if ($result) {
      while ($row = db_fetch_array($result)) {
        $mailup_fields[$row['mailup_field']]['fid'] = $row['fid'];
        $mailup_fields[$row['mailup_field']]['drupal_field'] = $row['name'];
      }
    }
    return $mailup_fields;
  } catch (MailUpException $exception) {
    watchdog('mailup', 'Service failed with exception: !ex', array('!ex' => $exception->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot initialize remote client. Please check your configuration.'),'error');
    return array();
  }
}

/**
 *  Update account email address for each subscription.
 *
 *  @param object $account
 *    The account being updated.
 *  @param string $new_email
 *    The new email address.
 *  @return boolean
 *    TRUE on success, FALSE otherwise.
 */
function mailup_update_email_address($account, $new_email) {
  $result = TRUE;
  // Read remote lists and refresh local storage.
  $lists = mailup_get_user_subscriptions($account);
  if (empty($lists)) {
    // We consider this successfull.
    return $result;
  }
  // Create a dummy account to perform unsubscriptions.
  $dummy = new stdClass();
  $dummy->mail = $new_email;
  foreach ($lists as $list) {
    mailup_unsubscribe($account, array($list));
    $result = $result && mailup_subscribe($dummy, array($list));
  }
  return $result;
}

/**
 *  Returns lists this user subscribed to.
 *
 *  @param object $account
 *    The user being checked.
 *  @return array
 *    The list array, or an empty array if user did not subscribe any list.
 */
function mailup_get_user_subscriptions($account) {
  $result = array();
  $lists = mailup_get_lists();
  if (empty($lists)) {
    return $result;
  }
  foreach ($lists as $list) {
    if (mailup_is_user_subscribed($account, $list)) {
      $result[] = $list;
    }
  }
  return $result;
}

/**
 *  Updates user profile fields on MailUp.
 *
 *  @param object $account
 *    The account to update.
 *  @return boolean
 *    Returns TRUE on success, FALSE otherwise.
 */
function mailup_update_user_profile($account) {
  $lists = mailup_get_user_subscriptions($account);
  if (empty($lists)) {
    return TRUE;
  }
  $mailup = new MailUpFrontend();
  $mailup->configure(mailup_remote_configuration());
  $use_confirm = variable_get('mailup_confirm', 0);
  $parameters = _mailup_subscribe_parameters($lists[0], $account, $use_confirm);
  $response = $mailup->invoke('XmlUpdSubscriber', $parameters);
  watchdog('mailup', "XmlUpdSubscriber sent for %user with code %code. Request query: %query", array('%user'=>$account->mail, '%code'=>$response['response'], '%query'=>$response['querystring']), WATCHDOG_NOTICE);
  return $response['response'] == 0;
}
