<?php
/**
 *  Page callback for user/%user/edit/mailup.
 *
 *  @param object $account
 *    The account being accessed.
 *  @return string
 *    Rendered subscription form.
 */
function mailup_subscription_edit($account) {
  $lists = mailup_local_lists(TRUE);
  if (empty($lists)) {
    return t('There are no lists to subscribe now');
  }
  foreach ($lists as &$list) {
    if (mailup_is_user_subscribed($account, $list)) {
      $list['checked'] = TRUE;
      $list['action'] ='mailup/unsubscribe/' . $list['id'] . '/' . $account->uid;
    } else {
      $list['checked'] = FALSE;
      $list['action'] = 'mailup/subscribe/' . $list['id'] . '/' . $account->uid;
    }
  }
  return theme('mailup_subscription', $lists);
}

/**
 *  Callback for 'mailup/subscribe' and 'mailup/unsubscribe'. Performs
 *  subscriptions and unsubscriptions on the current user.
 *
 *  @param array $list
 *    The list on which to perform the operation.
 *  @return string
 *    Content - not really. It performs drupal_goto on subscription page.
 */
function mailup_subscription_callback($list, $account) {
  if (arg(1) == 'subscribe') {
    $result = mailup_subscribe($account, array($list));
    if ($result) {
      if (variable_get('mailup_confirm', 0)) {
        drupal_set_message(t("Your subscription request has been sent, please check your email to confirm subscription."));
      } else {
        drupal_set_message(t('Your subscription to %list has been processed successfully.', array('%list' => $list['local_name']?$list['local_name']:$list['name'])));
      }
    } else {
      drupal_set_message(t('An error occurred during the subscription, please retry or contact an administrator if the problem persists.'), 'error');
    }
  } else if (arg(1) == 'unsubscribe') {
    $result = mailup_subscribe($account, array($list), 'unsubscribe');
    if ($result) {
      drupal_set_message(t('You are no longer subscribed to %list now.', array('%list' => $list['local_name']?$list['local_name']:$list['name'])));
    } else {
      drupal_set_message(t('An error occurred during the unsubscription, please retry or contact an administrator if the problem persists.'), 'error');
    }
  }
  drupal_goto("user/{$account->uid}/edit/mailup");
}
