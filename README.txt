Copyright 2012 - Fabrizio Ranieri

Module Description:
-------------------
This module provides integration between Drupal and MailUp (www.mailup.it).

Features:
---------
- Enable user subscription in registration form
- Enable/Disable confirm email service
- View your MailUp lists
- Add a new list
- Export your users in a MailUp-compliant format (XML or CSV), to manually
handle an import process in your MailUp Console
- Import your users into a MailUp list in 2 clicks: select the group you want
to import, then just hit "Import"
- Each user can control his own subscription status from the profile edit page.
The subscription status is read remotely, so it's always in sync

Requirements:
-------------
This is a Drupal 6 module. 
Profile must be enabled
PHP cURL and PHP SimpleXmlElement 
are needed too.

Installation:
-------------
See INSTALL.txt

Bugs/Features/Patches:
----------------------

